/*
Copyright (c) 2018 Hideki EIRAKU <hdk_2@users.sourceforge.net>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    (1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    (2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

    (3)The name of the author may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
#include <EfiCommon.h>
#include <EfiApi.h>
#include EFI_PROTOCOL_DEFINITION(LoadedImage)
#include EFI_PROTOCOL_DEFINITION(SimpleFileSystem)
#include EFI_PROTOCOL_DEFINITION(SimpleNetwork)
#include EFI_PROTOCOL_DEFINITION(ServiceBinding)
#include EFI_PROTOCOL_DEFINITION(Dhcp4)
#include EFI_PROTOCOL_DEFINITION(Mtftp4)
typedef struct{
  EFI_SYSTEM_TABLE*SystemTable;
  BOOLEAN DoGet,DoPut,DoRun;
  VOID*Buffer;
  UINTN BufferLength,ReadBytes;
  EFI_FILE_PROTOCOL*Root,*File;
  CHAR16*GetFileName;
}MTFTPC_CONTEXT;
EFI_GUID gEfiLoadedImageProtocolGuid=EFI_LOADED_IMAGE_PROTOCOL_GUID;
EFI_GUID gEfiSimpleFileSystemProtocolGuid=EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
EFI_GUID gEfiSimpleNetworkProtocolGuid=EFI_SIMPLE_NETWORK_PROTOCOL_GUID;
EFI_GUID gEfiDhcp4ServiceBindingProtocolGuid=EFI_DHCP4_SERVICE_BINDING_PROTOCOL_GUID;
EFI_GUID gEfiMtftp4ServiceBindingProtocolGuid=EFI_MTFTP4_SERVICE_BINDING_PROTOCOL_GUID;
EFI_GUID gEfiDhcp4ProtocolGuid=EFI_DHCP4_PROTOCOL_GUID;
EFI_GUID gEfiMtftp4ProtocolGuid=EFI_MTFTP4_PROTOCOL_GUID;
CHAR16*GetOneOption(CHAR16**Options,UINT32*OptionsLength){
  CHAR16*Option=*Options;
  BOOLEAN Space=FALSE,Quote=FALSE;
  for(;*OptionsLength;++*Options,--*OptionsLength){
    switch(**Options){
    case L'\0':
      *OptionsLength=0;
      return Option;
    case L' ':
      if(!Quote&&!Space)Space=TRUE,**Options=L'\0';
      break;
    case L'"':
      if(Quote){
	Quote=FALSE,Space=TRUE,**Options=L'\0';
	break;
      }else if(Option==*Options){
	Quote=TRUE,Option++;
	break;
      }
      /* Fall through */
    default:
      if(Space)return Option;
    }
  }
  return NULL;
}
int CompareStr(CHAR16*String1,CHAR16*String2){
  for(;;){
    int Ret=(int)*String1-(int)*String2;
    if(Ret||!*String1)return Ret;
    String1++,String2++;
  }
}
UINT32 StringToNumber(CHAR16*String,CHAR16**StringNext){
  BOOLEAN Minus=FALSE;
  UINT32 Base=10;
  *StringNext=String;
  if(*String==L'-')Minus=TRUE,String++;
  if(*String==L'0'){
    Base=8,String++;
    if(*String==L'X'||*String==L'x')Base=16,String++;
    else if(*String==L'B'||*String==L'b')Base=2,String++;
    else *StringNext=String;
  }
  UINT32 Number=0;
  for(;;){
    UINT32 Digit;
    if(*String>=L'0'&&*String<=L'9')Digit=*String-L'0';
    else if(*String>=L'A'&&*String<=L'F')Digit=*String-L'A'+10;
    else if(*String>=L'a'&&*String<=L'f')Digit=*String-L'a'+10;
    else break;
    if(Digit>=Base)break;
    if(Number>0xffffffff/Base)Number=0xffffffff;
    else Number=Number*Base+Digit;
    *StringNext=++String;
  }
  return Minus?-Number:Number;
}
EFI_STATUS GetIPAddress(CHAR16*String,EFI_IPv4_ADDRESS*IPAddress){
  UINT32 Address=0;
  for(UINT32 Dot=0;Dot<4;Dot++){
    CHAR16*StringNext;
    UINT32 Value=StringToNumber(String,&StringNext);
    if(String==StringNext)break;
    String=StringNext;
    if(*String==L'\0'){
      Address|=Value&(0xffffffffu>>(Dot*8));
      IPAddress->Addr[0]=Address>>24;
      IPAddress->Addr[1]=Address>>16;
      IPAddress->Addr[2]=Address>>8;
      IPAddress->Addr[3]=Address;
      return EFI_SUCCESS;
    }else if(*String==L'.'&&Dot<3){
      Address|=(Value&0xff)<<(24-Dot*8);
      String++;
    }else break;
  }
  return EFI_INVALID_PARAMETER;
}
VOID PrintError(EFI_SYSTEM_TABLE*SystemTable,EFI_STATUS Status,CHAR16*String){
  CHAR16 Buffer[]=L"Status[################]: ";
  for(UINTN i=0;i<16;i++)
    Buffer[7+i]=L"0123456789abcdef"[(Status>>((15-i)*4))&15];
  SystemTable->ConOut->OutputString(SystemTable->ConOut,Buffer);
  SystemTable->ConOut->OutputString(SystemTable->ConOut,String);
}
CHAR16*NumberToString(UINTN Number,CHAR16*BufferEnd){
  *BufferEnd=L'\0';
  do *--BufferEnd=L'0'+(Number%10);
  while((Number/=10));
  return BufferEnd;
}
VOID PrintReadBytes(EFI_SYSTEM_TABLE*SystemTable,UINTN ReadBytes,CHAR16*String){
  SystemTable->ConOut->OutputString(SystemTable->ConOut,String);
  CHAR16 Buffer[64];
  SystemTable->ConOut->OutputString(SystemTable->ConOut,NumberToString(ReadBytes,&Buffer[63]));
}
EFI_STATUS EFIAPI MtftpcCheckPacket(EFI_MTFTP4_PROTOCOL*This,EFI_MTFTP4_TOKEN*Token,UINT16 PacketLen,EFI_MTFTP4_PACKET*Packet){
  MTFTPC_CONTEXT*MtftpcContext=Token->Context;
  EFI_SYSTEM_TABLE*SystemTable=MtftpcContext->SystemTable;
  EFI_STATUS Status=EFI_SUCCESS;
  if(MtftpcContext->DoPut)return Status;
  union{
    UINT16 Word;
    UINT8 Byte[2];
  }OpCodeNet;
  OpCodeNet.Word=Packet->OpCode;
  UINT16 OpCode=((UINT16)OpCodeNet.Byte[0]<<8)|OpCodeNet.Byte[1];
  if(OpCode!=EFI_MTFTP4_OPCODE_DATA)return Status;
  UINT16 DataLength=PacketLen-4;
  VOID*Data=&Packet->Data.Data[0];
  BOOLEAN PrintProgress=FALSE;
  if(MtftpcContext->DoRun){
    VOID*Buffer;
    Status=SystemTable->BootServices->AllocatePool(EfiLoaderData,MtftpcContext->BufferLength+DataLength,&Buffer);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"AllocatePool failed\r\n");
      return Status;
    }
    if(MtftpcContext->BufferLength){
      SystemTable->BootServices->CopyMem(Buffer,MtftpcContext->Buffer,MtftpcContext->BufferLength);
      SystemTable->BootServices->FreePool(MtftpcContext->Buffer);
    }
    SystemTable->BootServices->CopyMem(Buffer+MtftpcContext->BufferLength,Data,DataLength);
    MtftpcContext->Buffer=Buffer;
    MtftpcContext->BufferLength+=DataLength;
    PrintProgress=TRUE;
  }else if(!MtftpcContext->GetFileName){
    if(!MtftpcContext->ReadBytes)SystemTable->ConOut->OutputString(SystemTable->ConOut,L"    \r");
    UINT8*Buffer=Data;
    for(UINTN BufferSize=DataLength;!EFI_ERROR(Status)&&BufferSize--;Buffer++){
      if(*Buffer=='\n')Status=SystemTable->ConOut->OutputString(SystemTable->ConOut,L"\r\n");
      else if(*Buffer>0&&*Buffer<0x80){
	CHAR16 String[2]={*Buffer};
	Status=SystemTable->ConOut->OutputString(SystemTable->ConOut,String);
      }
    }
  }else{
    if(!MtftpcContext->File){
      EFI_FILE_PROTOCOL*File;
      Status=MtftpcContext->Root->Open(MtftpcContext->Root,&File,MtftpcContext->GetFileName,EFI_FILE_MODE_READ|EFI_FILE_MODE_WRITE|EFI_FILE_MODE_CREATE,EFI_FILE_ARCHIVE);
      if(EFI_ERROR(Status)){
	PrintError(SystemTable,Status,L"Open failed\r\n");
	return Status;
      }
      MtftpcContext->File=File;
    }
    UINTN BufferSize=DataLength;
    Status=MtftpcContext->File->Write(MtftpcContext->File,&BufferSize,Data);
    if(!EFI_ERROR(Status)&&BufferSize!=DataLength)Status=EFI_VOLUME_FULL;
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Write failed\r\n");
      return Status;
    }
    PrintProgress=TRUE;
  }
  MtftpcContext->ReadBytes+=DataLength;
  if(PrintProgress)PrintReadBytes(SystemTable,MtftpcContext->ReadBytes,L"\rDownload ");
  return Status;
}
EFI_STATUS EFIAPI MtftpcPacketNeeded(EFI_MTFTP4_PROTOCOL*This,EFI_MTFTP4_TOKEN*Token,UINT16*Length,VOID**Buffer){
  MTFTPC_CONTEXT*MtftpcContext=Token->Context;
  EFI_SYSTEM_TABLE*SystemTable=MtftpcContext->SystemTable;
  EFI_STATUS Status=EFI_SUCCESS;
  if(MtftpcContext->DoGet)return Status;
  UINTN BufferSize=MtftpcContext->BufferLength;
  Status=MtftpcContext->File->Read(MtftpcContext->File,&BufferSize,MtftpcContext->Buffer);
  if(EFI_ERROR(Status)){
    PrintError(SystemTable,Status,L"Read failed\r\n");
    return Status;
  }
  *Length=BufferSize;
  *Buffer=MtftpcContext->Buffer;
  PrintReadBytes(SystemTable,MtftpcContext->ReadBytes+=BufferSize,L"\rUpload ");
  return Status;
}
UINT8*ConvertString(EFI_SYSTEM_TABLE*SystemTable,CHAR16*String){
  CHAR16*p;
  for(p=String;*p;p++){
    if(*p>=0x80){
      SystemTable->ConOut->OutputString(SystemTable->ConOut,L"US-ASCII only\r\n");
      return NULL;
    }
  }
  VOID*Ret;
  EFI_STATUS Status=SystemTable->BootServices->AllocatePool(EfiLoaderData,p-String+1,&Ret);
  if(EFI_ERROR(Status)){
    PrintError(SystemTable,Status,L"AllocatePool failed\r\n");
    return NULL;
  }
  UINT8*q;
  for(p=String,q=Ret;(*q++=*p++););
  return Ret;
}
EFI_STATUS EFIAPI EfiMain(EFI_HANDLE ImageHandle,EFI_SYSTEM_TABLE*SystemTable){
  VOID*VoidLoadedImageProtocol;
  EFI_STATUS Status=SystemTable->BootServices->OpenProtocol(ImageHandle,&gEfiLoadedImageProtocolGuid,&VoidLoadedImageProtocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
  if(EFI_ERROR(Status))return Status;
  EFI_LOADED_IMAGE_PROTOCOL*LoadedImageProtocol=VoidLoadedImageProtocol;
  if(!LoadedImageProtocol->LoadOptions)goto Usage;
  if(!LoadedImageProtocol->LoadOptionsSize)goto Usage;
  VOID*VoidOptions;
  Status=SystemTable->BootServices->AllocatePool(EfiLoaderData,LoadedImageProtocol->LoadOptionsSize+4,&VoidOptions);
  if(EFI_ERROR(Status))goto End;
  SystemTable->BootServices->CopyMem(VoidOptions,LoadedImageProtocol->LoadOptions,LoadedImageProtocol->LoadOptionsSize);
  SystemTable->BootServices->SetMem(VoidOptions+LoadedImageProtocol->LoadOptionsSize,4,0);
  CHAR16*Options=VoidOptions;
  UINT32 OptionsLength=LoadedImageProtocol->LoadOptionsSize/2+1;
  CHAR16*Args[5];
  UINT32 Argc;
  for(Argc=0;Argc<5;Argc++){
    Args[Argc]=GetOneOption(&Options,&OptionsLength);
    if(!Args[Argc])break;
  }
  if(Argc<4){
  Usage:
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Usage:\r\n"
				      "  mtftpc.efi get <IPv4 address> <remote filename> [<local filename>]\r\n"
				      "  mtftpc.efi put <IPv4 address> <local filename> [<remote filename>]\r\n"
				      "  mtftpc.efi exec <IPv4 address> <remote filename> [<arguments>]\r\n"
				      "  mtftpc.efi load <IPv4 address> <remote filename>\r\n");
    goto End2;
  }
  BOOLEAN DoGet=FALSE,DoPut=FALSE,DoRun=FALSE,DoExec=FALSE,NeedFile=FALSE;
  if(!CompareStr(Args[1],L"get"))DoGet=TRUE,NeedFile=TRUE;
  else if(!CompareStr(Args[1],L"put"))DoPut=TRUE,NeedFile=TRUE;
  else if(!CompareStr(Args[1],L"exec"))DoGet=TRUE,DoRun=TRUE,DoExec=TRUE;
  else if(!CompareStr(Args[1],L"load")&&Argc<5)DoGet=TRUE,DoRun=TRUE;
  else goto Usage;
  EFI_IPv4_ADDRESS IPAddress;
  Status=GetIPAddress(Args[2],&IPAddress);
  if(EFI_ERROR(Status)){
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Invalid IPv4 address\r\n");
    goto End2;
  }
  if(DoPut&&!CompareStr(Args[3],L"-")){
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Reading standard input is not supported\r\n");
    goto End2;
  }
  CHAR16*GetFileName=Argc<5?Args[3]:Args[4];
  if(DoGet&&!CompareStr(GetFileName,L"-"))GetFileName=NULL,NeedFile=FALSE;
  VOID*VoidSimpleFileSystemProtocol=NULL;
  if(NeedFile)Status=SystemTable->BootServices->OpenProtocol(LoadedImageProtocol->DeviceHandle,&gEfiSimpleFileSystemProtocolGuid,&VoidSimpleFileSystemProtocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
  if(EFI_ERROR(Status)){
    PrintError(SystemTable,Status,L"Could not find SimpleFileSystemProtocol\r\n");
    goto End2;
  }
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL*SimpleFileSystemProtocol=VoidSimpleFileSystemProtocol;
  EFI_FILE_PROTOCOL*Root=NULL;
  if(NeedFile)Status=SimpleFileSystemProtocol->OpenVolume(SimpleFileSystemProtocol,&Root);
  if(EFI_ERROR(Status)){
    PrintError(SystemTable,Status,L"OpenVolume failed\r\n");
    goto End3;
  }
  UINTN HandleCount;
  EFI_HANDLE *HandleBuffer;
  Status=SystemTable->BootServices->LocateHandleBuffer(ByProtocol,&gEfiSimpleNetworkProtocolGuid,NULL,&HandleCount,&HandleBuffer);
  if(EFI_ERROR(Status)){
    PrintError(SystemTable,Status,L"Could not find SimpleNetworkProtocol not found\r\n");
    goto End4;
  }
  BOOLEAN Completed=FALSE;
  for(UINTN HandleIndex=0;!Completed&&HandleIndex<HandleCount;HandleIndex++){
    VOID*VoidDhcp4ServiceBindingProtocol;
    Status=SystemTable->BootServices->OpenProtocol(HandleBuffer[HandleIndex],&gEfiDhcp4ServiceBindingProtocolGuid,&VoidDhcp4ServiceBindingProtocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
    if(EFI_ERROR(Status))continue;
    EFI_SERVICE_BINDING_PROTOCOL*Dhcp4ServiceBindingProtocol=VoidDhcp4ServiceBindingProtocol;
    VOID*VoidMtftp4ServiceBindingProtocol;
    Status=SystemTable->BootServices->OpenProtocol(HandleBuffer[HandleIndex],&gEfiMtftp4ServiceBindingProtocolGuid,&VoidMtftp4ServiceBindingProtocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
    if(EFI_ERROR(Status))goto OpenMtftp4SBFail;
    EFI_SERVICE_BINDING_PROTOCOL*Mtftp4ServiceBindingProtocol=VoidMtftp4ServiceBindingProtocol;
    EFI_HANDLE Dhcp4Handle=NULL;
    Status=Dhcp4ServiceBindingProtocol->CreateChild(Dhcp4ServiceBindingProtocol,&Dhcp4Handle);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4ServiceBindingProtocol->CreateChild failed\r\n");
      goto CreateDhcp4Fail;
    }
    VOID*VoidDhcp4Protocol;
    Status=SystemTable->BootServices->OpenProtocol(Dhcp4Handle,&gEfiDhcp4ProtocolGuid,&VoidDhcp4Protocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4Protocol open failed\r\n");
      goto OpenDhcp4Fail;
    }
    EFI_DHCP4_PROTOCOL*Dhcp4Protocol=VoidDhcp4Protocol;
    EFI_HANDLE Mtftp4Handle=NULL;
    Status=Mtftp4ServiceBindingProtocol->CreateChild(Mtftp4ServiceBindingProtocol,&Mtftp4Handle);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Mtftp4ServiceBindingProtocol->CreateChild failed\r\n");
      goto CreateMtftp4Fail;
    }
    VOID*VoidMtftp4Protocol;
    Status=SystemTable->BootServices->OpenProtocol(Mtftp4Handle,&gEfiMtftp4ProtocolGuid,&VoidMtftp4Protocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Mtftp4Protocol open failed\r\n");
      goto OpenMtftp4Fail;
    }
    EFI_MTFTP4_PROTOCOL*Mtftp4Protocol=VoidMtftp4Protocol;
    EFI_DHCP4_MODE_DATA Dhcp4ModeData;
    Status=Dhcp4Protocol->GetModeData(Dhcp4Protocol,&Dhcp4ModeData);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4 GetModeData failed\r\n");
      goto Dhcp4Fail;
    }
    if(Dhcp4ModeData.State==Dhcp4Bound)goto Dhcp4Done;
    EFI_DHCP4_CONFIG_DATA Dhcp4CfgData;
    Dhcp4CfgData.DiscoverTryCount=0;
    Dhcp4CfgData.DiscoverTimeout=NULL;
    Dhcp4CfgData.RequestTryCount=0;
    Dhcp4CfgData.RequestTimeout=NULL;
    Dhcp4CfgData.ClientAddress.Addr[0]=0;
    Dhcp4CfgData.ClientAddress.Addr[1]=0;
    Dhcp4CfgData.ClientAddress.Addr[2]=0;
    Dhcp4CfgData.ClientAddress.Addr[3]=0;
    Dhcp4CfgData.Dhcp4Callback=NULL;
    Dhcp4CfgData.OptionCount=0;
    Status=Dhcp4Protocol->Configure(Dhcp4Protocol,&Dhcp4CfgData);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4 Configure failed\r\n");
      goto Dhcp4Fail;
    }
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"DHCP\r");
    Status=Dhcp4Protocol->Start(Dhcp4Protocol,NULL);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4 Start failed\r\n");
      goto Dhcp4Fail;
    }
    Status=Dhcp4Protocol->GetModeData(Dhcp4Protocol,&Dhcp4ModeData);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Dhcp4 GetModeData failed\r\n");
      goto Dhcp4Fail;
    }
    if(Dhcp4ModeData.State!=Dhcp4Bound){
      SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Dhcp4 state is not bound\r\n");
      goto Dhcp4Fail;
    }
  Dhcp4Done:;
    EFI_MTFTP4_CONFIG_DATA MtftpConfigData;
    MtftpConfigData.UseDefaultSetting=FALSE;
    MtftpConfigData.StationIp=Dhcp4ModeData.ClientAddress;
    MtftpConfigData.SubnetMask=Dhcp4ModeData.SubnetMask;
    MtftpConfigData.LocalPort=0;
    MtftpConfigData.GatewayIp=Dhcp4ModeData.RouterAddress;
    MtftpConfigData.ServerIp=IPAddress;
    MtftpConfigData.InitialServerPort=0;
    MtftpConfigData.TryCount=3;
    MtftpConfigData.TimeoutValue=5;
    Status=Mtftp4Protocol->Configure(Mtftp4Protocol,&MtftpConfigData);
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Mtftp4 Configure failed\r\n");
      goto Mtftp4Fail;
    }
    MTFTPC_CONTEXT MtftpcContext;
    MtftpcContext.SystemTable=SystemTable;
    MtftpcContext.DoGet=DoGet;
    MtftpcContext.DoPut=DoPut;
    MtftpcContext.DoRun=DoRun;
    MtftpcContext.Buffer=NULL;
    MtftpcContext.BufferLength=0;
    MtftpcContext.ReadBytes=0;
    MtftpcContext.Root=Root;
    MtftpcContext.File=NULL;
    MtftpcContext.GetFileName=GetFileName;
    if(DoPut){
      EFI_FILE_PROTOCOL*File;
      Status=Root->Open(Root,&File,Args[3],EFI_FILE_MODE_READ,0);
      if(EFI_ERROR(Status)){
	PrintError(SystemTable,Status,L"Open failed\r\n");
	Completed=TRUE;
	goto OpenFail;
      }
      MtftpcContext.File=File;
      VOID*Buffer;
      Status=SystemTable->BootServices->AllocatePool(EfiLoaderData,512,&Buffer);
      if(EFI_ERROR(Status)){
	PrintError(SystemTable,Status,L"AllocatePool failed\r\n");
	Completed=TRUE;
	goto AllocateFail;
      }
      MtftpcContext.Buffer=Buffer;
      MtftpcContext.BufferLength=512;
    }
    EFI_MTFTP4_TOKEN Token;
    Token.Event=NULL;
    Token.OverrideData=NULL;
    Token.Filename=ConvertString(SystemTable,!DoPut||Argc<5?Args[3]:Args[4]);
    if(!Token.Filename)goto ConvertStringFail;
    Token.ModeStr=NULL;
    Token.OptionCount=0;
    Token.BufferSize=0;
    Token.Buffer=NULL;
    Token.Context=&MtftpcContext;
    Token.CheckPacket=MtftpcCheckPacket;
    Token.TimeoutCallback=NULL;
    Token.PacketNeeded=MtftpcPacketNeeded;
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"TFTP\r");
    if(DoPut)Status=Mtftp4Protocol->WriteFile(Mtftp4Protocol,&Token);
    else Status=Mtftp4Protocol->ReadFile(Mtftp4Protocol,&Token);
    SystemTable->ConOut->OutputString(SystemTable->ConOut,L"\r\n");
    if(EFI_ERROR(Status)){
      PrintError(SystemTable,Status,L"Transfer failed\r\n");
      goto TransferFail;
    }else Completed=TRUE;
    if(DoRun){
      if(!MtftpcContext.Buffer){
	SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Buffer null (empty file?)\r\n");
	goto LoadFail;
      }
      EFI_HANDLE NewImageHandle=NULL;
      Status=SystemTable->BootServices->LoadImage(FALSE,ImageHandle,NULL,MtftpcContext.Buffer,MtftpcContext.BufferLength,&NewImageHandle);
      if(EFI_ERROR(Status)){
	PrintError(SystemTable,Status,L"LoadImage failed\r\n");
	goto LoadDone;
      }
      VOID*VoidNewLoadedImageProtocol;
      Status=SystemTable->BootServices->OpenProtocol(NewImageHandle,&gEfiLoadedImageProtocolGuid,&VoidNewLoadedImageProtocol,ImageHandle,NULL,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
      if(EFI_ERROR(Status)){
	PrintError(SystemTable,Status,L"Could not open LoadedImageProtocol\r\n");
	goto LoadDone;
      }
      EFI_LOADED_IMAGE_PROTOCOL*NewLoadedImageProtocol=VoidNewLoadedImageProtocol;
      if(DoExec){
	CHAR16*OptionsStart=VoidOptions;
	UINTN NewOptionsOffset=(Args[3]-OptionsStart)*sizeof(CHAR16);
	if(LoadedImageProtocol->LoadOptionsSize>NewOptionsOffset){
	  NewLoadedImageProtocol->LoadOptions=LoadedImageProtocol->LoadOptions+NewOptionsOffset;
	  NewLoadedImageProtocol->LoadOptionsSize=LoadedImageProtocol->LoadOptionsSize-NewOptionsOffset;
	}
      }
      EFI_MEMORY_TYPE NewImageCodeType=NewLoadedImageProtocol->ImageCodeType;
      SystemTable->BootServices->CloseProtocol(NewImageHandle,&gEfiLoadedImageProtocolGuid,ImageHandle,NULL);
      if(DoExec&&NewImageCodeType!=EfiLoaderCode){
	SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Not application\r\n");
	goto LoadDone;
      }
      if(!DoExec&&NewImageCodeType!=EfiBootServicesCode&&NewImageCodeType!=EfiRuntimeServicesCode){
	SystemTable->ConOut->OutputString(SystemTable->ConOut,L"Not driver\r\n");
	goto LoadDone;
      }
      Status=SystemTable->BootServices->StartImage(NewImageHandle,NULL,NULL);
      if(EFI_ERROR(Status))PrintError(SystemTable,Status,L"StartImage failed\r\n");
      NewImageHandle=NULL;
    LoadDone:
      if(NewImageHandle)SystemTable->BootServices->UnloadImage(NewImageHandle);
    }
  LoadFail:
  TransferFail:
    SystemTable->BootServices->FreePool(Token.Filename);
  ConvertStringFail:
    if(MtftpcContext.Buffer)SystemTable->BootServices->FreePool(MtftpcContext.Buffer);
  AllocateFail:
    if(MtftpcContext.File)MtftpcContext.File->Close(MtftpcContext.File);
  OpenFail:
  Mtftp4Fail:
  Dhcp4Fail:
    SystemTable->BootServices->CloseProtocol(Mtftp4Handle,&gEfiMtftp4ProtocolGuid,ImageHandle,NULL);
  OpenMtftp4Fail:
    Mtftp4ServiceBindingProtocol->DestroyChild(Mtftp4ServiceBindingProtocol,Mtftp4Handle);
  CreateMtftp4Fail:
    SystemTable->BootServices->CloseProtocol(Dhcp4Handle,&gEfiDhcp4ProtocolGuid,ImageHandle,NULL);
  OpenDhcp4Fail:
    Dhcp4ServiceBindingProtocol->DestroyChild(Dhcp4ServiceBindingProtocol,Dhcp4Handle);
  CreateDhcp4Fail:
    SystemTable->BootServices->CloseProtocol(HandleBuffer[HandleIndex],&gEfiMtftp4ServiceBindingProtocolGuid,ImageHandle,NULL);
  OpenMtftp4SBFail:
    SystemTable->BootServices->CloseProtocol(HandleBuffer[HandleIndex],&gEfiDhcp4ServiceBindingProtocolGuid,ImageHandle,NULL);
  }
  SystemTable->BootServices->FreePool(HandleBuffer);
 End4:
  if(NeedFile)Root->Close(Root);
 End3:
  if(NeedFile)SystemTable->BootServices->CloseProtocol(LoadedImageProtocol->DeviceHandle,&gEfiSimpleFileSystemProtocolGuid,ImageHandle,NULL);
 End2:
  SystemTable->BootServices->FreePool(VoidOptions);
 End:
  SystemTable->BootServices->CloseProtocol(ImageHandle,&gEfiLoadedImageProtocolGuid,ImageHandle,NULL);
  return Status;
}
