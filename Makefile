edk2dir=edk2
CC=x86_64-w64-mingw32-gcc
CFLAGS=-Wall -O -shared -e EfiMain@8 -mno-red-zone -mno-sse -nostdinc -nostdlib -ffreestanding -fno-builtin -fno-stack-protector -fno-strict-aliasing -Wl,--subsystem,10 -I$(edk2dir)/EdkCompatibilityPkg/Foundation/Include -I$(edk2dir)/EdkCompatibilityPkg/Foundation/Include/X64 -I$(edk2dir)/EdkCompatibilityPkg/Foundation/Efi/Include -I$(edk2dir)/EdkCompatibilityPkg/Foundation/Efi -I$(edk2dir)/EdkCompatibilityPkg/Foundation/Framework/Include
mtftpc.efi:
.SUFFIXES:.efi .c
.c.efi:
	$(CC) $(CFLAGS) -o $@ $<
